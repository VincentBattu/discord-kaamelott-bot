import { Command } from './command'
import { Message, MessageEmbed, VoiceChannel } from 'discord.js';
import lunr, { Index } from 'lunr'
import { SoundsLoader, Sound } from './sounds-loader'
import { SoundPlayer } from './sound-player'
import { EMBED_COLOR, NOT_IN_VOICE_CHANNEL_MESSAGE } from './constants'

export class PlayCommand implements Command {

  public name = 'play'

  public description = `Lis une citation. La recherche fonctionne textuellement ou par index.
  !!play <citation> ou !!play <id>
  Exemple: !!play la blague est pas drôle`

  private lunrIndex!: Index

  private sounds: Sound[] = []

  public constructor (private soundsLoader: SoundsLoader, private soundPlayer: SoundPlayer) {
  }

  public async onInit (): Promise<void> {
    const sounds = await this.soundsLoader.getSoundsData()
    this.sounds = sounds
    this.lunrIndex = lunr(function() {
      this.ref('name')
      this.field('text')

      sounds.forEach(sound => {
        this.add({
          name: sound.file,
          text: sound.title
        })
      })   
    })
  }

  public async execute(message: Message): Promise<void> {
    const voiceChannel = message.member?.voice.channel

    if (!voiceChannel) {
      message.reply(NOT_IN_VOICE_CHANNEL_MESSAGE)
      return
    }
    
    const splittedMessage = message.content.split(' ')
    if (splittedMessage.length === 2) {
      const index = parseInt(splittedMessage[1])
      if (!isNaN(index)) {
        if (index > this.sounds.length) {
          message.reply("Ton index n'est pas valide")
          return
        }
        const sound = this.sounds[index]
        return this.soundPlayer.play(voiceChannel, this.soundsLoader.getSoundPath(sound.file))
      }
    }

    const firstSpaceIdx = message.content.indexOf(' ')
    const search = message.content.substr(firstSpaceIdx).trim().split(' ').map(el => {
      if (el.length > 3) {
        return el + '~1'
      }
      return el
    }).join(' ')

    const result = this.lunrIndex.search(search  + '~1')
    if (result.length === 0) {
      message.channel.send('Auncun résultat trouvé pour cette recherche')
      return
    }
    const filename = result[0].ref
    
    const relevantSoundIdx = this.sounds.findIndex(sound => sound.file === filename)
    message.channel.send(`Lecture de la citation ${relevantSoundIdx}`)
    if (result.length > 1) {
      let embed = new MessageEmbed()
        .setTitle("Ces résultats pourraient t'intéresser")
        .setColor(EMBED_COLOR)
      for (let i = 1; i < Math.min(result.length, 6); i++) {
        const soundIdx = this.sounds.findIndex(sound => sound.file === result[i].ref)
        if (soundIdx !== -1) {
          embed = embed.addField(soundIdx, this.sounds[soundIdx].title)
        }
      }
      message.channel.send(embed)
    }

    await this.soundPlayer.play(voiceChannel, this.soundsLoader.getSoundPath(filename))
  }
}
