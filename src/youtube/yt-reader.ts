import { Message, MessageEmbed, StreamDispatcher, VoiceConnection } from 'discord.js'
import internal from 'stream'
import ytdl from 'ytdl-core-discord'
import ytpl from 'ytpl'
import { URL } from 'url'
import { EMBED_COLOR, NOT_IN_VOICE_CHANNEL_MESSAGE } from '../constants'

interface QueueElement {
  nextMusics: { url: string, title: string}[]
  voiceConnection: VoiceConnection | null
  streamDispatcher: StreamDispatcher | null
}

export class YtReader {
  private queues: Map<string, QueueElement> = new Map()

  public async addToQueue (youtubeUrl: string, message: Message): Promise<void> {
    const voiceChannel = message.member?.voice.channel
    if (!voiceChannel) {
      message.reply(NOT_IN_VOICE_CHANNEL_MESSAGE)
      return
    }

    const guildId = message.guild!.id
    if (!this.queues.has(guildId)) {
      this.queues.set(guildId, {
        nextMusics: [],
        voiceConnection: null,
        streamDispatcher: null
      })
    }
    const queueElement = this.queues.get(guildId)!

    const url = new URL(youtubeUrl)

    const hasListQueryParam = url.searchParams.has('list')
    if (hasListQueryParam) {
      const resultBatch = await ytpl(youtubeUrl, { limit: Infinity })

      queueElement.nextMusics.push(
        ...resultBatch.items.map(item => ({ url: item.url, title: item.title }))
      )
      return
    }

    const valid = ytdl.validateURL(youtubeUrl)
    if (!valid) {
      message.reply('URL invalide')
      return
    }

    const info = await ytdl.getBasicInfo(youtubeUrl)
    const title = info.videoDetails.title
    
    queueElement.nextMusics.push({
      title,
      url: youtubeUrl
    })

    if (queueElement.nextMusics.length > 1 || (queueElement.nextMusics.length === 1 && queueElement.voiceConnection !== null)) {
      const embed = new MessageEmbed()
        .setColor(EMBED_COLOR)
        .setDescription(`**${title}** ajouté à la queue (position ${queueElement.nextMusics.length})`)
      await message.channel.send(embed)
    }
  }

  public async play (message: Message) {
    const voiceChannel = message.member?.voice.channel

    if (!voiceChannel) {
      message.reply(NOT_IN_VOICE_CHANNEL_MESSAGE)
      return
    }
    const guildId = message.guild!.id
    const queueElement = this.queues.get(guildId)!

    if (!queueElement.voiceConnection) {
      queueElement.voiceConnection = await voiceChannel.join()
    }

    const queueData = await this.getNextQueueData(queueElement)
    if (!queueData) {
      this.stop(message)
      return
    }

    queueElement.streamDispatcher = queueElement.voiceConnection.play(queueData.stream, { type: 'opus' })
      .on('start', async () => {
          const embed = new MessageEmbed()
            .setColor(EMBED_COLOR)
            .setDescription(`Tu écoutes **${queueData.title}**`)
            await message.channel.send(embed)
      })  
      .on('finish', async () => {
        if (!this.hasNextMusicInQueue(queueElement)) {
          queueElement.voiceConnection!.disconnect()
          queueElement.voiceConnection = null
          queueElement.streamDispatcher = null
        } else {
          await this.wait(5_000)
          await this.play(message)
        }
      })
  }

  public pause (message: Message) {
    const guildId = message.guild!.id
    if (!this.isPlaying(guildId)) {
      message.reply("Aucune musique n'est actuellement lue")
      return
    }
    const streamDispatcher = this.queues.get(guildId)?.streamDispatcher
    
    if (streamDispatcher) {
      streamDispatcher.pause()
    }
  }

  public resume (message: Message) {
    const guildId = message.guild!.id
    if (!this.isPlaying(guildId)) {
      message.reply("Aucune musique n'est actuellement lue")
      return
    }
    const streamDispatcher = this.queues.get(guildId)?.streamDispatcher
    
    if (streamDispatcher) {
      streamDispatcher.resume()
    }
  }

  public stop (message: Message) {
    const guildId = message.guild!.id
    if (!this.isPlaying(guildId)) {
      message.reply("Aucune musique n'est actuellement lue")
      return
    }

    const queueElement = this.queues.get(guildId)
    if (queueElement) {
      queueElement.voiceConnection?.disconnect()
      queueElement.nextMusics = []
      queueElement.streamDispatcher = null
      queueElement.voiceConnection = null
    }
  }

  public isPlaying (guildId: string) {
    return (this.queues.get(guildId)?.voiceConnection || null) !== null
  }

  public getNextMusicTitles (guildId: string, numberOfMusics: number): string[] {
    const nextMusics = this.queues.get(guildId)?.nextMusics || []
    const result: string[] = []
    for (let i = 0; i < Math.min(numberOfMusics, nextMusics.length); i++) {
      result.push(nextMusics[i].title)
    }
    return result
  }

  public getQueueSize (guildId: string,) {
    return this.queues.get(guildId)?.nextMusics.length || 0
  }

  private async getNextQueueData (queueElement: QueueElement): Promise<{ stream: internal.Readable, title: string } | null> {
    const musicToRead = queueElement.nextMusics.shift()
    if (!musicToRead) {
      return null
    }
    return {
      title: musicToRead.title,
      stream: await ytdl(musicToRead.url),
    }
  }

  private hasNextMusicInQueue (queueElement: QueueElement) {
    return queueElement.nextMusics.length > 0
  }

  private async wait (ms: number): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      setTimeout(() => resolve(), ms)
    })
  }
}
