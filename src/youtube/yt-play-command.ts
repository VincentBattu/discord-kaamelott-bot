import { Message } from 'discord.js'
import { Command } from '../command'
import { YtReader } from './yt-reader'

export class YtPlayCommand implements Command {
  public name = 'yt-play'

  public description = 'Lis une vidéo youtube'

  public constructor (private readonly ytReader: YtReader) {
  }

  public async onInit(): Promise<void> {
  }

  public async execute(message: Message): Promise<void> {
    const splittedMessage = message.content.split(' ')
    if (splittedMessage.length !== 2) {
      message.reply(`Format de la commande invalide. Utilise: !!${this.name} <url> `)
    }
    const youtubeUrl = splittedMessage[1]

    await this.ytReader.addToQueue(youtubeUrl, message)
    if (!this.ytReader.isPlaying(message.guild!.id)) {
      await this.ytReader.play(message)
    }
  }
}