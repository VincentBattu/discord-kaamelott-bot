import { Message } from 'discord.js'
import { Command } from '../command'
import { NOT_IN_VOICE_CHANNEL_MESSAGE } from '../constants'
import { YtReader } from './yt-reader'

export class YtStopCommand implements Command {  
  public name = 'yt-stop'
  public description = 'Déconnecte le bot'

  public constructor (private readonly ytReader: YtReader) {
  }

  public async onInit(): Promise<void> {
  }

  public async execute(message: Message): Promise<void> {
    const voiceChannel = message.member?.voice.channel

    if (!voiceChannel) {
      message.reply(NOT_IN_VOICE_CHANNEL_MESSAGE)
      return
    }

    await this.ytReader.stop(message)

    message.reply('Ok boomer')
  }
}