import { Message } from 'discord.js'
import { Command } from '../command'
import { NOT_IN_VOICE_CHANNEL_MESSAGE } from '../constants'
import { YtReader } from './yt-reader'

export class YtResumeCommand implements Command {  
  public name = 'yt-resume'
  public description = 'Relance la lecture d\'une musique mise en pause'

  public constructor (private readonly ytReader: YtReader) {
  }

  public async onInit(): Promise<void> {
  }

  public async execute(message: Message): Promise<void> {
    const voiceChannel = message.member?.voice.channel

    if (!voiceChannel) {
      message.reply(NOT_IN_VOICE_CHANNEL_MESSAGE)
      return
    }

    this.ytReader.resume(message)
  }
}