import { Message, MessageEmbed } from 'discord.js'
import { Command } from '../command'
import { EMBED_COLOR, NOT_IN_VOICE_CHANNEL_MESSAGE } from '../constants'
import { YtReader } from './yt-reader'

export class YtListNextCommand implements Command {  
  public name = 'yt-ls-next'
  public description = 'Liste les 5 prochaines musiques dans la queue'

  public constructor (private readonly ytReader: YtReader) {
  }

  public async onInit(): Promise<void> {
  }

  public async execute(message: Message): Promise<void> {
    const voiceChannel = message.member?.voice.channel

    if (!voiceChannel) {
      message.reply(NOT_IN_VOICE_CHANNEL_MESSAGE)
      return
    }

    const guildId = message.guild!.id
    const queueSize = this.ytReader.getQueueSize(guildId)
    if (queueSize === 0) {
      message.reply('La queue est vide ;)')
      return
    }

    const nextMusics = this.ytReader.getNextMusicTitles(message.guild!.id, 5)

    const embed = new MessageEmbed()
      .setTitle("Prochaines musiques")
      .setColor(EMBED_COLOR)
      .setFooter(`${queueSize} musique${queueSize > 1 ? 's' : ''} restante${queueSize > 1 ? 's' : ''}`)
    
    for (let i = 0; i < nextMusics.length; i++) {
      embed.addField(i + 1, nextMusics[i])
    }
    message.channel.send(embed)
  }
}