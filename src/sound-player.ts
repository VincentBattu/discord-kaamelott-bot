import { VoiceChannel } from 'discord.js'

export class SoundPlayer {

  public async play (voiceChannel: VoiceChannel, soundPath: string): Promise<void> {
    const connection = await voiceChannel.join()
    try {
      await this.wait(400)
      const dispatcher = connection.play(soundPath)
      await new Promise((resolve) => dispatcher.on('finish', () => resolve()))
      await this.wait(300)
    } finally {
      await voiceChannel.leave()
    }
  }

  private wait (milli: number): Promise<void> {
    return new Promise((resolve) => setTimeout(() => resolve(), milli))
  }
}
