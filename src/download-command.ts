import { Command } from './command'
import { Message, MessageAttachment } from 'discord.js'
import { SoundsLoader } from './sounds-loader'

export class DownloadCommand implements Command {
  public name = 'download'
  public description = 'Télécharge un fichier répertoriant toutes les citations'
  
  public constructor (private soundsLoader: SoundsLoader) {
  }

  public async onInit (): Promise<void> {
  }

  public async execute (message: Message): Promise<void> {
    const result = ''

    message.channel.send('', {
      files: [
        new MessageAttachment(new Buffer('coucou'), 'test.txt')
      ]
    })
  }
}