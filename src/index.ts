import { Client } from 'discord.js'
import { Command } from './command'
import { ListCommand } from './list-command'
import { PlayCommand } from './play-command'
import { HelpCommand } from './help-command'
import { SoundsLoader } from './sounds-loader'
import { RandomCommand } from './random-command'
import { SoundPlayer } from './sound-player'
import { YtPlayCommand } from './youtube/yt-play-command'
import { YtReader } from './youtube/yt-reader'
import { YtPauseCommand } from './youtube/yt-pause-command'
import { YtResumeCommand } from './youtube/yt-resume-command'
import { YtSkipCommand } from './youtube/yt-skip-command'
import { YtStopCommand } from './youtube/yt-stop-command'
import { YtListNextCommand } from './youtube/yt-list-next-command'
import lunr from 'lunr'

const client = new Client()

const soundsLoader = new SoundsLoader()
const soundPlayer = new SoundPlayer()
const ytReader = new YtReader()

export const commands: Command[] = [
  new ListCommand(),
  new PlayCommand(soundsLoader, soundPlayer),
  new RandomCommand(soundsLoader, soundPlayer),
  new YtPlayCommand(ytReader),
  new YtPauseCommand(ytReader),
  new YtResumeCommand(ytReader),
  new YtSkipCommand(ytReader),
  new YtStopCommand(ytReader),
  new YtListNextCommand(ytReader)
];


/**
 * Entry point
 */
(async () => {
  commands.push(new HelpCommand(commands))

  const lunrIndex = lunr(function () {
    this.ref('name')
    this.field('name')
  
    commands.forEach(command => {
      this.add({
        name: command.name
      })
    })
  })

  for (const command of commands) {
    try {
      await command.onInit()
    } catch (e) {
      console.error(`Fail to init command ${command.constructor.name}`)
      console.error(e)
    }
  }
  
  client.on('message', async message => {
    if (message.content.startsWith('!!')) {
      const commandName = message.content.substr(2).split(' ')[0]
      
      let commandFound = false
      for (const command of commands) {
        if (command.name === commandName) {
          commandFound = true
          try {
            await command.execute(message)
          } catch (e) {
            console.error(`Fail to execute command ${command.constructor.name}`)
            console.error(e)
          }
          break
        }
      }

      if (!commandFound) {
        const result = lunrIndex.search(commandName  + '~1')
        if (result.length !== 0) {
          message.reply(`Commande invalide, voulais-tu utiliser **!!${result[0].ref}** ?`)
        } else {
          message.reply('Commande invalide')
        }
      }
    }
  })
  
  client.on('ready', () => {
    client.user?.setActivity({ name: '!!help'})
    console.log(`Logged in as ${client.user!.tag}!`);
  });
  
  client.login('NzQxNjMwNjMyNzEwMzczMzk2.Xy6XWQ.g83A6DC_V418uzLbfsdiEq2Wlxk')  
})()
