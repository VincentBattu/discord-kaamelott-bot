import { Command } from './command'
import { Message } from 'discord.js'
import { SoundsLoader } from './sounds-loader'
import { SoundPlayer } from './sound-player'
import { NOT_IN_VOICE_CHANNEL_MESSAGE } from './constants'

export class RandomCommand implements Command {
  public name = 'rand'
  public description = 'Lance une citation de manière aléatoire.'

  public constructor (private soundsLoader: SoundsLoader, private soundPlayer: SoundPlayer) {
  }
  
  public async onInit (): Promise<void> {
  }

  public async execute (message: Message): Promise<void> {
    const voiceChannel = message.member?.voice.channel

    if (!voiceChannel) {
      message.reply(NOT_IN_VOICE_CHANNEL_MESSAGE)
      return
    }

    const sounds = await this.soundsLoader.getSoundsData()

    const randomIndex = Math.floor(Math.random() * sounds.length)
    const randomSound = sounds[randomIndex]
    
    message.channel.send(`Lecture de la citation ${randomIndex}`)

    await this.soundPlayer.play(voiceChannel, this.soundsLoader.getSoundPath(randomSound.file))
  }
}