import { Message } from "discord.js";

export interface Command {
  onInit (): Promise<void>
  execute (message: Message): Promise<void>
  
  readonly name: string
  readonly description: string
}
