import { readFile } from 'fs'
import { resolve } from 'path'

export interface Sound {
  character: string
  episode: string
  file: string
  title: string
}

export class SoundsLoader {

  private sounds: Sound[] | null= null

  public async getSoundsData (): Promise<Sound[]> {
    if (this.sounds === null) {
      this.sounds = await this.loadSoundFile() 
    }
    return this.sounds
  }

  public getSoundPath (soundFileName: string): string {
    return resolve(__dirname, `../sounds/${soundFileName}`)
  }

  private loadSoundFile (): Promise<Sound[]> {
    const soundsFile = resolve(__dirname, '../sounds/sounds.json')

    return new Promise((resolve, reject) => {
      readFile(soundsFile, 'utf8', (err, data) => {
        if (err) {
          reject(err)
        } else {
          resolve(JSON.parse(data))
        }
      })
    })
  }
}
