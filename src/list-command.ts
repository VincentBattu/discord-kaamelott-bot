import { Command } from './command'
import { Message, MessageEmbed } from 'discord.js';

export class ListCommand implements Command {
  
  public name = 'list'

  public description = 'Liste les citations disponibles'

  public async onInit(): Promise<void> {

  }

  public async execute(message: Message): Promise<void> {
    const embed = new MessageEmbed()
      // Set the title of the field
      .setTitle('A slick little embed')
      // Set the color of the embed
      .setColor(0x0099ff)
      // Set the main content of the embed
      .setDescription(``)
      .addField('1', 'Un', true)
      .addField('2', 'Deux', true)
      .addField('3', 'Trois', true)
    // Send the embed to the same channel as the message
    message.channel.send(embed);
  }

}