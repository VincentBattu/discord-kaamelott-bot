import { Message, MessageEmbed } from 'discord.js'
import { Command } from './command'
import { EMBED_COLOR } from './constants'

export class HelpCommand implements Command {
  
  public readonly name = 'help'

  public readonly description = 'Aide'

  public constructor (private commands: Command[]) {
  }

  public async onInit(): Promise<void> {
  }

  public async execute (message: Message): Promise<void> {
    let embed = new MessageEmbed()
      .setTitle("Commands disponibles")
      .setColor(EMBED_COLOR)
    const sortedCommands = this.commands.sort((a, b) => a.name.localeCompare(b.name))
    for (const command of sortedCommands) {
      embed = embed.addField(command.name, command.description)
    }
    message.channel.send(embed)
  }

}

